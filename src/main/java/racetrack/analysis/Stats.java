/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.util.ArrayList;
import java.util.List;

import racetrack.framework.Bundles;
import racetrack.framework.BundlesUtils;

/**
 *
 */
public class Stats {
  /**
   * Calculate a pearson correlation from two fields within a bundle set -- those fields
   * either need to be scalar values or they need to be float values.
   *
   *@param bundles record set
   *@param f0      scalar or float field
   *@param f1      scalar or float field
   *
   *@return pearson correlation coefficient
   */
  public static double pearsonCorrelationCoefficient(Bundles bundles, String f0, String f1) {
    List<String> fields = new ArrayList<String>(); fields.add(f0); fields.add(f1);
    double d[][] = BundlesUtils.asDoubleArray(bundles, fields);
    return pearsonCorrelationCoefficient(d[0], d[1]);
  }

  /**
   * Calculate a perason correlation coefficient from two arrays of doubles.
   *
   * From:  https://wikipedia.org/wiki/Pearson_correlation_coefficient
   *
   *@param d0 first array -- needs to have the same number of values as the second array
   *@param d1 second array -- needs to have the same number of values as the first array
   *
   *@return pearson correlation coefficient
   */
  public static double pearsonCorrelationCoefficient(double d0[], double d1[]) {
    // Calculate averages first
    double d0_a = 0.0, d1_a = 0.0; int n = d0.length;
    for (int i=0;i<n;i++) { d0_a += d0[i]; d1_a += d1[i]; }
    d0_a /= n; d1_a /= n;

    // Now the standard deviation
    double d0_sd = 0.0, d1_sd = 0.0;
    for (int i=0;i<n;i++) { d0_sd += (d0[i] - d0_a)*(d0[i] - d0_a); d1_sd += (d1[i] - d1_a)*(d1[i] - d1_a); }
    d0_sd = Math.sqrt(d0_sd); d1_sd = Math.sqrt(d1_sd);
    
    // Now the pearson correlation
    double sum = 0.0;
    for (int i=0;i<n;i++) sum += (d0[i] - d0_a) * (d1[i] - d1_a);

    return sum / (d0_sd * d1_sd);
  }


  /**
   * Test method for pearson correlation.
   */
  public static void main(String args[]) {
    // Example from www.statisticshowto.com/how-to-computre-pearsons-correlation-coefficients/
    double age[]     = { 43.0, 21.0, 25.0, 42.0, 57.0, 59.0 },
           glucose[] = { 99.0, 65.0, 79.0, 75.0, 87.0, 81.0 };
    double pearson   = pearsonCorrelationCoefficient(age, glucose);
    System.out.println("Pearson Calc = " + pearson + " ... should be " + (0.529809));
  }
}

