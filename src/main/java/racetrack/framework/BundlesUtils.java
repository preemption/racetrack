/* 

Copyright 2013 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

import racetrack.gui.RT;
import racetrack.util.CSVReader;
import racetrack.util.Utils;

/**
 * Utilities for the classes in this file.
 *
 * @author  D. Trimm 
 * @version 1.0
 */
public class BundlesUtils {
  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  file       file to parse
   * @param  max_lines  maximum number of lines to parse, 0 indicates unlimited
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, File file, int max_lines) { return parse(bundles, null, file, null, max_lines); }

  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  rt         application class
   * @param  file       file to parse
   * @param  appconfigs lines from the parsed file that may indicate application configuration information (output)
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, RT rt, File file, List<String> appconfs) { return parse(bundles, rt, file, appconfs, 0); }

  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  rt         application class
   * @param  file       file to parse
   * @param  appconfigs lines from the parsed file that may indicate application configuration information (output)
   * @param  max_lines  maximum number of lines to parse, 0 indicates unlimited
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, RT rt, File file, List<String> appconfs, int max_lines) {
    // Determine if the delimiter is commas, tabs, or pipes
    BufferedReader in = null; Map<String,Map<Integer,Integer>> map = new HashMap<String,Map<Integer,Integer>>();
    map.put(",",  new HashMap<Integer,Integer>()); map.put("|",  new HashMap<Integer,Integer>()); map.put("\t", new HashMap<Integer,Integer>());
    try {
      if (file.getName().toLowerCase().endsWith(".gz")) in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file)))); 
      else                                              in = new BufferedReader(new FileReader(file)); 
      
      // Parse the first ten lines or so to determine the delimiter
      String line; int i = 0;
      while ((line = in.readLine()) != null && line.equals("") == false && i < 10) {
        i++;
        int commas = count(line, ','), pipes  = count(line, '|'), tabs   = count(line, '\t');
        // System.err.println("commas=" + commas + " pipes=" + pipes + " tabs=" + tabs + " \"" + line + "\"");
        if (commas > 0) { if (!map.get(",").containsKey(commas))  map.get(",").put(commas, 0);  map.get(",").put(commas,  map.get(",").get(commas)  + 1); }
        if (pipes  > 0) { if (!map.get("|").containsKey(pipes))   map.get("|").put(pipes,  0);  map.get("|").put(pipes,   map.get("|").get(pipes)   + 1); }
        if (tabs   > 0) { if (!map.get("\t").containsKey(tabs))   map.get("\t").put(tabs,  0);  map.get("\t").put(tabs,   map.get("\t").get(tabs)   + 1); }
      }
    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
      ioe.printStackTrace(System.err);
    } finally {
      if (in != null) try { in.close(); } catch (IOException ioe) { } 
    }

    // Determine the strongest pattern for the delimiter -- checks for comma, pipe, and tab
    String delims = ","; int c_strength, p_strength, t_strength;
    if (map.get(","). keySet().size() == 1) { c_strength = map.get(","). get(map.get(","). keySet().iterator().next()); } else c_strength = -1;
    if (map.get("|"). keySet().size() == 1) { p_strength = map.get("|"). get(map.get("|"). keySet().iterator().next()); } else p_strength = -1;
    if (map.get("\t").keySet().size() == 1) { t_strength = map.get("\t").get(map.get("\t").keySet().iterator().next()); } else t_strength = -1;

    if      (c_strength > p_strength && c_strength > t_strength) delims = ",";
    else if (p_strength > c_strength && p_strength > c_strength) delims = "|";
    else if (t_strength > p_strength && t_strength > c_strength) delims = "\t";
    // System.err.println("c=" + c_strength + " | p=" + p_strength + " | t=" + t_strength); // DEBUG

    // Create the set for storage
    Set<Bundle> set = new HashSet<Bundle>();
    // Execute the parser
    CSVParser csv_parser = null;
    try { 
      // System.err.println("Creating CSVReader..."); // DEBUG
      CSVReader reader = new CSVReader(file, csv_parser = new CSVParser(bundles, rt, set, max_lines), delims, true); 
      // System.err.println("  CSVReader Complete!"); // DEBUG
    } catch (IOException ioe) { 
      System.err.println("IOException: " + ioe); ioe.printStackTrace(System.err); 
    }
    // Make sure that the class had a chance to add the lists to main class
    if (csv_parser != null) { csv_parser.addListsToRT(); if (appconfs != null) appconfs.addAll(csv_parser.getAppConfigs()); }
    // Re-run the transforms for the fast lookup tables
    bundles.getGlobals().resetTransforms();
    return set;
  }

  /**
   * Count the number of character occurences within a string.
   *
   *@param str string to examine
   *@param c   charater to count
   *
   *@return number of times character occurs in string
   */
  public static int count(String str, char c) {
    int sum = 0;
    for (int i=0;i<str.length();i++) if (str.charAt(i) == c) sum++;
    return sum;
  }

  /**
   * Return a deduped version of the bundle set that is passed in.
   *
   *@param orig original set of records -- probably contains duplicates
   *
   *@return record set not containing duplicates
   */
  public static Set<Bundle> dedupe(Set<Bundle> orig) {
    Set<Bundle> ret = new HashSet<Bundle>(); Set<String> already_seen = new HashSet<String>();

    Iterator<Bundle> it = orig.iterator(); while (it.hasNext()) {
      Bundle bundle         = it.next();
      String canonical_form = canonicalStringFormat(bundle);
      if (already_seen.contains(canonical_form) == false) { ret.add(bundle); already_seen.add(canonical_form); }
    }

    return ret;
  }

  /**
   * Create a single string representing the bundle -- rep is used for deduping.
   *
   *@param bundle
   *
   *@return string that can be used to identify duplicate bundles
   */
  public static String canonicalStringFormat(Bundle bundle) {
    StringBuffer sb = new StringBuffer(); // contains the increment string

    Tablet tablet = bundle.getTablet(); sb.append(tablet.fileHeader());
    int flds[] = tablet.getFields();
    for (int i=0;i<flds.length;i++) { if (flds[i] >= 0) sb.append(BundlesDT.DELIM + Utils.encToURL(bundle.toString(flds[i]))); }
    if (tablet.hasTimeStamps()) sb.append(BundlesDT.DELIM + Utils.exactDate(bundle.ts0()));
    if (tablet.hasDurations())  sb.append(BundlesDT.DELIM + Utils.exactDate(bundle.ts1()));

    return sb.toString();
  }

  /**
   * Convert a scalar or float field into a double array.
   *
   *@param bundles records
   *@param field   field in bundles -- either a
   */
  public static double[] asDoubleArray(Bundles bundles, String field) {
    // Figure out which tablets have the field
    List<String> strs = new ArrayList<String>(); Map<String,Tablet> tabs = new HashMap<String,Tablet>();  int sum = 0;
    Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next();
      if (KeyMaker.tabletCompletesBlank(tablet, field)) { tabs.put("" + tablet, tablet); strs.add("" + tablet); sum += tablet.size(); }
    }

    // Sort them to be consistent
    Collections.sort(strs);

    // Allocate the array
    double d[] = new double[sum]; int d_i = 0;

    // Add the values into the array
    BundlesG globals = bundles.getGlobals();
    for (int i=0;i<strs.size();i++) {
      Tablet   tablet = tabs.get(strs.get(i));
      KeyMaker km     = new KeyMaker(tablet, field);

      Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(globals.fieldIndex(field));
      if        (datatypes.contains(BundlesDT.DT.FLOAT)   && datatypes.size() == 1) {
        Iterator<Bundle> it = tablet.bundleIterator(); while (it.hasNext()) {
	  int ints[] = km.intKeys(it.next());
	  d[d_i++] = Float.intBitsToFloat(ints[0]);
	}
      } else if (datatypes.contains(BundlesDT.DT.INTEGER) && datatypes.size() == 1) {
        Iterator<Bundle> it = tablet.bundleIterator(); while (it.hasNext()) {
	  int ints[] = km.intKeys(it.next());
	  d[d_i++] = ints[0];
	}
      } else throw new RuntimeException("Field \"" + field + "\" is not integer or float");
    }

    return d;
  }

  /**
   * Convert a scalar or float field into a set of double arrays.  This method keeps the
   * elements paired with their same bundle.
   *
   *@param bundles records
   *@param fields  list of fields -- will be the order of the arrays
   */
  public static double[][] asDoubleArray(Bundles bundles, List<String> fields) {
    // Figure out which tablets have the field
    int size = 0; List<Tablet> tablets = new ArrayList<Tablet>(); Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
      Tablet  tablet        = it_tab.next();
      boolean all_satisfied = true;
      for (int i=0;i<fields.size();i++) if (KeyMaker.tabletCompletesBlank(tablet, fields.get(i)) == false) all_satisfied = false;
      if (all_satisfied) { tablets.add(tablet); size += tablet.size(); }
    }

    // Determine how to parse the fields
    boolean parse_float[] = new boolean[fields.size()]; BundlesG globals = bundles.getGlobals();
    for (int i=0;i<fields.size();i++) {
      Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(globals.fieldIndex(fields.get(i)));
      if      (datatypes.contains(BundlesDT.DT.FLOAT)   && datatypes.size() == 1) parse_float[i] = true;
      else if (datatypes.contains(BundlesDT.DT.INTEGER) && datatypes.size() == 1) parse_float[i] = false;
      else throw new RuntimeException("Field \"" + fields.get(i) + "\" is not integer or float");
    }

    // Create the arrays
    double d[][] = new double[fields.size()][size];

    // Fill the arrays
    int index = 0;
    for (int i_tab=0;i_tab<tablets.size();i_tab++) {
      // Create key makers for all of the fields
      KeyMaker kms[] = new KeyMaker[fields.size()];
      for (int i=0;i<kms.length;i++) kms[i] = new KeyMaker(tablets.get(i_tab), fields.get(i));

      // Go through the records
      Iterator<Bundle> it = tablets.get(i_tab).bundleIterator(); while (it.hasNext()) {
        Bundle bundle = it.next();
	for (int i=0;i<fields.size();i++) {
	  double value = 0.0; 

	  int ints[] = kms[i].intKeys(bundle);

	  // The assumption should be that every record has a filled in field... but we'll check anyway...
	  if (ints != null && ints.length > 0) {
	    if (parse_float[i]) value = Float.intBitsToFloat(ints[0]);
	    else                value = ints[0];
          }

          d[i][index] = value;
	}
	index++;
      }
    }

    return d;
  }
}

