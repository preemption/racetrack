/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Color;
import java.awt.Graphics2D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class ClusterColors {
  /**
   *
   */
  private static final long serialVersionUID = -1209743209804082L;

  /**
   * Construct the class.
   *
   */
  public ClusterColors(BrewerColorScale scales[], Set<Double> values) {
    double as_array[] = new double[values.size()];
    Iterator<Double> it = values.iterator();
    for (int i=0;i<as_array.length;i++) as_array[i] = it.next();
    generic(scales, as_array);
  }

  /**
   * Construct the class.
   *
   */
  public ClusterColors(BrewerColorScale scales[], double values[]) { generic(scales, values); }

  /**
   * Generic constructor.
   *
   *@param scales color scales to use -- determines the number of clusters
   *@param values values that need to be assigned to colors
   */
  protected void generic(BrewerColorScale scales[], double values[]) {
    int n = scales.length;

    // Run a k-means cluster
    double            centers[] = new double[n];
    List<Set<Double>> clusters  = new ArrayList<Set<Double>>();
    for (int i=0;i<n;i++)              clusters.add(new HashSet<Double>());
    for (int i=0;i<values.length;i++)  { int index = (int) ((Math.random() * (Integer.MAX_VALUE-1))%n); clusters.get(index).add(values[i]); centers[index] += values[i]; }

    // Do a certain number of iterations
    final int iterations = 30;
    for (int its=0;its<iterations;its++) {
      // Make the centers
      for (int i=0;i<n;i++) {
        centers[i] = 0.0; int samples = 0; Iterator<Double> itd = clusters.get(i).iterator(); while (itd.hasNext()) { centers[i] += itd.next(); samples++; }
	if (samples == 0) samples = 1; centers[i] = centers[i]/samples;
	clusters.get(i).clear();
      }

      // Assign each value to a cluster based on distance
      for (int i=0;i<values.length;i++) {
        int closest_index = 0; double closest_d = Math.abs(values[i] - centers[0]);
	for (int center=1;center<centers.length;center++) {
	  double d = Math.abs(values[i] - centers[center]);
	  if (d < closest_d) { closest_d = d; closest_index = center; }
	}
	clusters.get(closest_index).add(values[i]);
      }
    }

    // Recalculate the centers
    for (int i=0;i<n;i++) {
      centers[i] = 0.0; int samples = 0; Iterator<Double> itd = clusters.get(i).iterator(); while (itd.hasNext()) { centers[i] += itd.next(); samples++; }
      if (samples == 0) samples = 1; centers[i] = centers[i]/samples;
    }

    // Make a lookup for the centers
    Map<Double,Set<Double>> center_to_set = new HashMap<Double,Set<Double>>();
    for (int i=0;i<centers.length;i++) center_to_set.put(centers[i], clusters.get(i));

    // Sort the centers
    Arrays.sort(centers);

    // Assign the values to colors
    for (int i_scale=0;i_scale<scales.length;i_scale++) {
      Set<Double> set = center_to_set.get(centers[i_scale]); // clusters.get(i_scale);

      // Find min and max
      double min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
      Iterator<Double> it = set.iterator(); while (it.hasNext()) { double d = it.next(); if (min > d) min = d; if (max < d) max = d; }
      
      // Assign colors
      it = set.iterator(); while (it.hasNext()) { 
        double d = it.next(); float norm = (float) ((d - min)/(max - min));
        double_to_color.put(d, scales[i_scale].at(norm));
      }
    }
  }

  /**
   * Maps double values to color values
   */
  private Map<Double,Color> double_to_color = new HashMap<Double,Color>();

  /**
   * Render the clustered colors as a color scale.
   *
   *@param g2d graphics primitive
   *@param x   x coordinate for colorscale
   *@param y   y coordinate for colorscale
   *@param w   width of the colorscale
   *@param h   height of the colorscale
   */
  public void renderColorScale(Graphics2D g2d, int x, int y, int w, int h) {
    // Find the min and max first
    double min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
    Iterator<Double> it = double_to_color.keySet().iterator(); while (it.hasNext()) {
      double d = it.next(); if (d < min) min = d; if (d > max) max = d;
    }

    // Make an array of colors and fill them in with the exact values
    Color colors[] = new Color[w];
    it = double_to_color.keySet().iterator(); while (it.hasNext()) {
      double d = it.next(); Color c = double_to_color.get(d);
      int    i = (int) ((w * (d - min))/(max - min));
      if (i >= colors.length) i = colors.length - 1;
      colors[i] = c;
    }

    // Expand the colors in the array about 5 times...
    for (int k=0;k<5;k++) {
      for (int i=0;i<colors.length;i++) {
        // Find empty spots and look at their neighbors
        if (colors[i] == null) {
          int j = i - 1; if (j >= 0 && colors[j] != null) colors[i] = colors[j];
	      j = i + 1;
	  if (colors[i] == null && j < colors.length && colors[j] != null) colors[i] = colors[j];
	}
      }
    }

    // Render the expanded array
    for (int i=0;i<colors.length;i++) {
      if (colors[i] != null) { g2d.setColor(colors[i]); g2d.drawLine(x + i, y, x + i, y + h); }
    }
  }

  /**
   * Return the color at the specific value.  The value passed must have existed in the constructed array.
   *
   *@param d value
   *
   *@return color at that value
   */
  public Color at(double d) { return double_to_color.get(d); }


  /**
   * Test method.
   */
  public static void main(String args[]) {
    // Create test data
    Set<Double> set = new HashSet<Double>();
    for (int i=0;i<100;i++) {
      double bin = Math.random();
      if        (bin < 0.333) { set.add(Math.random() * 10.0 + 5.0);
      } else if (bin < 0.666) { set.add(Math.random() * 30.0 + 100.0);
      } else                  { set.add(Math.random() * 50.0 + 500.0);
      }
    }

    // Create the scales
    BrewerColorScale scales[] = new BrewerColorScale[3];
    for (int i=0;i<scales.length;i++) scales[i] = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 7, i);

    // Run the algorithm
    ClusterColors cluster_colors = new ClusterColors(scales, set);

    // Print the results
    Iterator<Double> it = set.iterator(); while (it.hasNext()) {
      double d = it.next();
      System.out.println("Color @ " + d + " = " + cluster_colors.at(d));
    }
  }
}

