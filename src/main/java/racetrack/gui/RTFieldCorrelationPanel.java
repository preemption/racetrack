/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import racetrack.analysis.Stats;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesG;
import racetrack.framework.Tablet;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.RTColorManager;

import racetrack.util.Utils;

/**
 * Class that implements correlations with fields.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTFieldCorrelationPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -7263118719792372L;

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTFieldCorrelationPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   

    // Make the GUI
    add("Center",  component = new RTFieldCorrelationComponent());

    // Force a configure
    newRootBundles(getRTParent().getRootBundles());
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "fieldcorrelation"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTFieldCorrelationPanel";
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTFieldCorrelationPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTFieldCorrelationPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * For each field, list the other fields it should be compared against
   */
  Map<String,Set<String>> compare_lu = new HashMap<String,Set<String>>();

  /**
   * Convert a field name to its integer index
   */
  Map<String,Integer> field_to_index = new HashMap<String,Integer>();

  /**
   * Convert a field index to its field name
   */
  Map<Integer,String> index_to_field = new HashMap<Integer,String>();

  /**
   * Lookup that indicates a field is categorical
   */
  Map<String,Boolean> field_is_categorical = new HashMap<String,Boolean>();

  /**
   * Sorted list of fields
   */
  List<String> fields_sort = new ArrayList<String>();

  /**
   * Calculate the new settings for the new root.
   */
  public void newRootBundles(Bundles new_root) {
    BundlesG globals = new_root.getGlobals();
    
    // Clear the data structures
    compare_lu.clear(); field_to_index.clear(); index_to_field.clear(); fields_sort.clear();
    Set<String> fields_set = new HashSet<String>();

    // Go through the tablets -- figure out which fields to compare and how to compare them
    Iterator<Tablet> it_tab = new_root.tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet   = it_tab.next();

      // Get the fields and go over each combination - make a note of how to compare them
      int    fields[] = tablet.getFields();
      for (int i=0;i<fields.length;i++) { if (fields[i] < 0) continue;
        String f_i = globals.fieldHeader(i); field_to_index.put(f_i, i); index_to_field.put(i, f_i); fields_set.add(f_i);
	if (compare_lu.containsKey(f_i) == false) compare_lu.put(f_i, new HashSet<String>());

	field_is_categorical.put(f_i, categoricalField(i, f_i, globals));

        for (int j=0;j<fields.length;j++) {
	  if (i == j || fields[j] < 0) continue;

	  String f_j = globals.fieldHeader(j); field_to_index.put(f_j, j); index_to_field.put(j, f_j); fields_set.add(f_j);
	  if (compare_lu.containsKey(f_j) == false) compare_lu.put(f_j, new HashSet<String>());
	  compare_lu.get(f_j).add(f_i); compare_lu.get(f_i).add(f_j);
	
	  field_is_categorical.put(f_j, categoricalField(j, f_j, globals));
	}
      }
    }

    // Make a sorted list
    fields_sort.addAll(fields_set);
    Collections.sort(fields_sort, new Comparator<String>() { public int compare(String s1, String s2) {
      return (s1.toLowerCase()).compareTo(s2.toLowerCase());
    } } );
  }

  /**
   * Determine if a field should be treated as categorical (versus scalar).
   *
   *@param index   field index
   *@param name    field name
   *@param globals data structure globals
   *
   *@return true if the field is categorical
   */
  protected boolean categoricalField(int index, String name, BundlesG globals) {
    if (globals.isScalar(index)) { return false; } else {
      Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(index);
      if (datatypes.contains(BundlesDT.DT.FLOAT) && datatypes.size() == 1) return false;
      else                                                                 return true;
    }
  }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTFieldCorrelationComponent extends RTComponent {
    private static final long serialVersionUID = 1200123808410802L;

    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      return shapes; }

    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy();

      // Create the render context based on the user's parameters
      RenderContext myrc = null;
      if (bs != null) { myrc = new RenderContext(id, bs, count_by, color_by, getWidth(), getHeight()); }
      return myrc;
    }
    
    /**
     * RenderContext implementation for the correlation matrix
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by;

      /**
       * Pearson correlation coefficient for scalar to scalar comparisons
       */
      Map<String,Map<String,Double>> pearsons = new HashMap<String,Map<String,Double>>();

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
	this.count_by           = count_by;
	this.color_by           = color_by;

        int i = 0;
	while (currentRenderID() == getRenderID() && i < fields_sort.size()) {
	  int j = 0; String f_i = fields_sort.get(i);
	  while (currentRenderID() == getRenderID() && j < fields_sort.size()) {
            String f_j = fields_sort.get(j); 

	    if (f_i.compareTo(f_j) < 0 && compare_lu.get(f_i).contains(f_j)) { // Only compare half the matrix and mirror
	      if        (field_is_categorical.get(f_i) && field_is_categorical.get(f_j)) {

	      } else if (field_is_categorical.get(f_i))                                  {

	      } else if (                                 field_is_categorical.get(f_j)) {

	      } else                                                                     { 
	        double coefficient = Stats.pearsonCorrelationCoefficient(bs, f_i, f_j);
		if (pearsons.containsKey(f_i) == false) pearsons.put(f_i, new HashMap<String,Double>());
		pearsons.get(f_i).put(f_j, coefficient);
	      }
	    }

	    j++;
	  }
	  i++;
	}
      }

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (base_bi == null) {
	 Graphics2D g2d = null;
	 try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
	  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	  RTColorManager.renderVisualizationBackground(base_bi, g2d);

	  // Font metrics
          int txt_h = Utils.txtH(g2d, "0") + 4; int max_field_w = 6;
	  for (int i=0;i<fields_sort.size();i++) { int w = Utils.txtW(g2d,fields_sort.get(i)) + 6; if (w > max_field_w) max_field_w = w; }

	  // Geometry
	  int table_x = max_field_w,
	      table_y = max_field_w;

	  // Render the labels
	  Map<String,Integer> field_to_x = new HashMap<String,Integer>(), field_to_y = new HashMap<String,Integer>();
	  g2d.setColor(RTColorManager.getColor("label", "default"));
	  for (int i=0;i<fields_sort.size();i++) {
	    g2d.drawString(fields_sort.get(i), 2, table_y + txt_h * (i + 1));                       field_to_y.put(fields_sort.get(i), table_y + txt_h * i);
	    Utils.drawRotatedString(g2d, fields_sort.get(i), table_x + txt_h * (i+1), max_field_w); field_to_x.put(fields_sort.get(i), table_x + txt_h * i);
	  }

	  // Render the pearson coefficients
	  BrewerColorScale diverge_cs = new BrewerColorScale(BrewerColorScale.BrewerType.DIVERGING, 11);

          Iterator<String> it_f1 = pearsons.keySet().iterator(); while (it_f1.hasNext()) {
	    String f1 = it_f1.next(); Iterator<String> it_f2 = pearsons.get(f1).keySet().iterator(); while (it_f2.hasNext()) {
	      String f2 = it_f2.next(); double coefficient = pearsons.get(f1).get(f2);

	      if (coefficient < 0.0) { g2d.setColor(diverge_cs.atIndex((int) (5 - 5 * coefficient))); 
	      } else                 { g2d.setColor(diverge_cs.atIndex((int) (5 + 5 * coefficient))); }

	      g2d.fillRect(field_to_x.get(f1), field_to_y.get(f2), txt_h - 1, txt_h - 1);
	      g2d.fillRect(field_to_x.get(f2), field_to_y.get(f1), txt_h - 1, txt_h - 1);
	    }
	  }
	 } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }
  }
}

