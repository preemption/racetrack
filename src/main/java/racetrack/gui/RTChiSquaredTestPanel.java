/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.image.BufferedImage;

import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.Utils;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.ClusterColors;
import racetrack.visualization.ColorScale;
import racetrack.visualization.RTColorManager;

/**
 * Class that implements a chi-squared test between two categorical fields.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTChiSquaredTestPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -7263116325912933362L;

  /**
   * Radio button indicating that the first colorscale should be used (grayscale)
   */
  private JRadioButtonMenuItem colorscale_1_rbmi,
  /**
   * Radio button indicating that the second colorscale should be used (rainbow)
   */
                               colorscale_2_rbmi,
  /**
   * Radio button indicating that the third colorscale should be used (rainbow - equally spaced - requires sort)
   */
                               colorscale_3_rbmi,
  /**
   * Radio button indicating that the fourth colorscale should be used (ranged clusters - experimental)
   */
                               colorscale_4_rbmi,
  /**
   * Render the regular chi-square test values (expected-observed)*(expected-observed)/expected
   */
			       render_chi_square_test_rbmi,
  /**
   * Render the difference -- will create a smaller dynamic range
   */
			       render_difference_rbmi,
  /**
   * Render the expected value only (does not vary based on the data...)
   */
			       render_expected_rbmi;


  /**
   * Draw labels checkbox menu item
   */
  private JCheckBoxMenuItem    draw_labels_cbmi;

  /**
   * First field to use
   */
  public JComboBox field_0_cb,

  /**
   * Second field to use
   */
                   field_1_cb;

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTChiSquaredTestPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   
    // Make the GUI
    add("Center",  component = new RTChiSquaredTestComponent());

    // Make the field choices
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(field_0_cb = new JComboBox());
    panel.add(field_1_cb = new JComboBox());
    add("South", panel);

    // Update the menu
    // - Options for which value to calculate
    ButtonGroup bg = new ButtonGroup();
    getRTPopupMenu().add(render_chi_square_test_rbmi = new JRadioButtonMenuItem("Chi Square Test", true)); bg.add(render_chi_square_test_rbmi); defaultListener(render_chi_square_test_rbmi);
    getRTPopupMenu().add(render_difference_rbmi      = new JRadioButtonMenuItem("Difference"));            bg.add(render_difference_rbmi);      defaultListener(render_difference_rbmi);
    getRTPopupMenu().add(render_expected_rbmi        = new JRadioButtonMenuItem("Expected"));              bg.add(render_expected_rbmi);        defaultListener(render_expected_rbmi);

    getRTPopupMenu().addSeparator();

    // - Colorscale options
    bg = new ButtonGroup();
    getRTPopupMenu().add(colorscale_1_rbmi = new JRadioButtonMenuItem("Gray Scale", true)); bg.add(colorscale_1_rbmi); defaultListener(colorscale_1_rbmi);
    getRTPopupMenu().add(colorscale_2_rbmi = new JRadioButtonMenuItem("Rainbow"));          bg.add(colorscale_2_rbmi); defaultListener(colorscale_2_rbmi);
    getRTPopupMenu().add(colorscale_3_rbmi = new JRadioButtonMenuItem("Rainbow (Equal)"));  bg.add(colorscale_3_rbmi); defaultListener(colorscale_3_rbmi);
    getRTPopupMenu().add(colorscale_4_rbmi = new JRadioButtonMenuItem("Ranged Clusters"));  bg.add(colorscale_4_rbmi); defaultListener(colorscale_4_rbmi);

    getRTPopupMenu().addSeparator();

    // - Other rendering options
    getRTPopupMenu().add(draw_labels_cbmi = new JCheckBoxMenuItem("Draw Labels", true)); defaultListener(draw_labels_cbmi);

    defaultListener(field_0_cb);
    defaultListener(field_1_cb);

    updateBys();
  }

  /**
   * Update the comboboxes with the fields.
   */
  public void updateBys() {
    Bundles bs = getRTParent().getRootBundles();
    String blanks[] = KeyMaker.blanks(bs.getGlobals(), false, true, false, true);
    updateBys(field_0_cb, blanks);
    updateBys(field_1_cb, blanks);
  }
  
  /**
   * Update a single combobox.
   */
  protected void updateBys(JComboBox cb, String blanks[]) {
    String str = (String) cb.getSelectedItem();
    cb.removeAllItems(); 
    for (int i=0;i<blanks.length;i++) {
      if (blanks[i].equals(KeyMaker.TABLET_SEP_STR)   ||
          blanks[i].equals(KeyMaker.ALL_ENTITIES_STR) ||
	  blanks[i].equals(KeyMaker.BY_STRAIGHT_STR)  ||
          blanks[i].equals(KeyMaker.BY_AUTOTIME_STR)  ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_STR)          ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_STR)     ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_STR) ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_SEC_STR)) {
      } else cb.addItem(blanks[i]);
    }
    if (str != null) cb.setSelectedItem(str);
  }

  /**
   * Different render values
   */
  enum RENDER_VALUE { CHI_SQUARE, DIFFERENCE, EXPECTED };

  /**
   * Return the current render value.
   *
   *@return render value
   */
  public RENDER_VALUE renderValue() {
    if      (render_chi_square_test_rbmi.isSelected()) return RENDER_VALUE.CHI_SQUARE;
    else if (render_difference_rbmi     .isSelected()) return RENDER_VALUE.DIFFERENCE;
    else if (render_expected_rbmi       .isSelected()) return RENDER_VALUE.EXPECTED;
    else                                               return RENDER_VALUE.CHI_SQUARE;
  }

  /**
   * Set the render value.
   *
   *@param str new render value
   */
  public void renderValue(String str) {
    if      (str.equals("" + RENDER_VALUE.CHI_SQUARE)) render_chi_square_test_rbmi.setSelected(true);
    else if (str.equals("" + RENDER_VALUE.DIFFERENCE)) render_difference_rbmi.     setSelected(true);
    else if (str.equals("" + RENDER_VALUE.EXPECTED))   render_expected_rbmi.       setSelected(true);
  }

  /**
   * Return setting for drawing the labels.
   *
   *@return true to draw labels
   */
  public boolean drawLabels() { return draw_labels_cbmi.isSelected(); }

  /**
   * Set the option for drawing the labels.
   *
   *@param b true to draw the labels
   */
  public void drawLabels(boolean b) { draw_labels_cbmi.setSelected(b); }

  /**
   * Get the first field.
   *
   *@return first field
   */
  public String field0() { return (String) field_0_cb.getSelectedItem(); }

  /**
   * Set the first field.
   *
   *@param str new field value
   */
  public void field0(String str) { field_0_cb.setSelectedItem(str); }

  /**
   * Get the second field.
   *
   *@return second field
   */
  public String field1() { return (String) field_1_cb.getSelectedItem(); }

  /**
   * Set the second field.
   *
   *@param str new field value
   */
  public void field1(String str) { field_1_cb.setSelectedItem(str); }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "chisquared"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTChiSquaredTestPanel" +                             BundlesDT.DELIM +
           "field0="               + Utils.encToURL(field0()) +  BundlesDT.DELIM +
	   "field1="               + Utils.encToURL(field1()) +  BundlesDT.DELIM +
	   "draw_labels="          + drawLabels()             +  BundlesDT.DELIM +
	   "render_value="         + Utils.encToURL("" + renderValue());
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTChiSquaredTestPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTChiSquaredTestPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";

      if      (type.equals("field0"))       field0(Utils.decFmURL(value));
      else if (type.equals("field1"))       field1(Utils.decFmURL(value));
      else if (type.equals("draw_labels"))  drawLabels(value.toLowerCase().equals("true"));
      else if (type.equals("render_value")) renderValue("" + Utils.decFmURL(value));
      else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Force the component to re-look at the root bundles if it changes.
   */
  @Override
  public void newBundlesRoot(Bundles new_root) { ((RTChiSquaredTestComponent) getRTComponent()).resetSorts(); }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTChiSquaredTestComponent extends RTComponent {
    private static final long serialVersionUID = 184232535393118261L;

    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      set.addAll(myrc.geom_to_skey.keySet());
      return set; }

    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Bundle> it = bundles.iterator();
      while (it.hasNext()) {
        Bundle bundle = it.next();
        if (myrc.bundle_to_skeys.containsKey(bundle)) {
	  Iterator<String> its = myrc.bundle_to_skeys.get(bundle).iterator();
	  while (its.hasNext()) shapes.add(myrc.skey_to_geom.get(its.next()));
        }
      }
      return shapes; }

    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      String skey = myrc.geom_to_skey.get(shape);
      if (skey != null) { set.addAll(myrc.skey_to_bundles.get(skey)); }
      return set; }

    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      Iterator<Rectangle2D> it = myrc.geom_to_skey.keySet().iterator();
      while (it.hasNext()) {
        Rectangle2D rect = it.next();
	if (Utils.genericIntersects(rect, shape)) set.add(rect);
      }
      return set; }

    /**
     * Draw the closest point interaction - in this case, just specify the row and column that are drawn.
     */
    @Override
    public void addGarnish(Graphics2D g2d, int mx, int my) {
      RenderContext myrc = (RenderContext) rc; if (myrc != null) { boolean found = false;
        Iterator<Rectangle2D> it = myrc.geom_to_skey.keySet().iterator(); while (it.hasNext() && found == false) {
	  Rectangle2D rect = (Rectangle2D) it.next();
	  Rectangle2D rect_larger = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth() + 1, rect.getHeight() + 1); // make it one bigger because of the border
	  if (rect_larger.contains(mx,my)) {
	    found = true; String skey = myrc.geom_to_skey.get(rect);
	    StringTokenizer st = new StringTokenizer(skey, BundlesDT.DELIM);
	    String row = Utils.decFmURL(st.nextToken()),
	           col = Utils.decFmURL(st.nextToken());
	    // g2d.setColor(RTColorManager.getColor("label", "default"));
	    // g2d.drawString(row + " x " + col, mx, my);
            int x0    = (int) rect.getX(),
	        y0    = (int) rect.getY(),
		x1    = (int) (rect.getX() + rect.getWidth()),
		y1    = (int) (rect.getY() + rect.getHeight());
            int w_max = myrc.table_x + field1_sort.size() * myrc.cell_w,
	        h_max = myrc.table_y + field0_sort.size() * myrc.cell_h;

            Composite orig_comp = g2d.getComposite();
	    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
            g2d.setColor(RTColorManager.getColor("background", "default"));
	    g2d.fillRect(0,  0,  x0 - 1,      y0    - 1);
	    g2d.fillRect(0,  y1, x0 - 1,      h_max - y1);
	    g2d.fillRect(x1, 0,  w_max - x1,  y0    - 1);
	    g2d.fillRect(x1, y1, w_max - x1,  h_max - y1);
	    g2d.setComposite(orig_comp);
	  }
	}
      }
    }

    /**
     * Set value for field0 (rows)
     */
    String field0_set = null,

    /**
     * Set value for field1 (cols)
     */
           field1_set = null;

    /**
     * Sorted list of entities from field0
     */
    List<String> field0_sort = new ArrayList<String>(),

    /**
     * Sorted list of entities from field1
     */
                 field1_sort = new ArrayList<String>();

    /**
     * Validate the sorts.  If they are inconsistent, create them again.
     */
    protected synchronized void validateSorts(String f0, String f1) {
      if (f0.equals(field0_set) == false) { fill(field0_sort, getRTParent().getRootBundles(), f0); field0_set = f0; }
      if (f1.equals(field1_set) == false) { fill(field1_sort, getRTParent().getRootBundles(), f1); field1_set = f1; }
    }

    /**
     * Reset the sorts so that they are recalculated.
     */
    public void resetSorts() { field0_set = null; field1_set = null; }

    /**
     * Fill the list with all the unique values that make up a field.
     */
    protected void fill(List<String> list, Bundles root, String field) {
      boolean all_ints = true; // Determines how to sort values
      Set<String> set = new HashSet<String>();
      Iterator<Tablet> it_tab = root.tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next(); if (KeyMaker.tabletCompletesBlank(tablet, field)) {
	  KeyMaker km = new KeyMaker(tablet, field);
	  Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
	    Bundle bundle = it_bun.next(); String keys[] = km.stringKeys(bundle); if (keys != null) {
	      for (int i=0;i<keys.length;i++) {
                if (all_ints && Utils.isInteger(keys[i]) == false) all_ints = false;
                set.add(Utils.encToURL(keys[i]));
              }
	    }
	  }
	}
      }
      list.clear(); list.addAll(set); 
      if (all_ints) Collections.sort(list, new IntegerStringComparator());
      else          Collections.sort(list);
    }

    /**
     * Sorter for strings that are actually just decimal integers.
     */
    class IntegerStringComparator implements Comparator<String> {
      public int compare(String s1, String s2) {
        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);
	if      (i1 < i2) return -1;
	else if (i1 > i2) return  1;
	else              return  0;
      }
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy(),
		 field_0   = field0(),
		 field_1   = field1();

      // Render value
      RENDER_VALUE render_value = renderValue();

      // Decode the appropriate colorscale
      ColorScale cs = null; boolean equal_cs = false;
      if      (colorscale_1_rbmi.isSelected()) { cs = RTColorManager.getContinuousColorScale(); }
      else if (colorscale_2_rbmi.isSelected()) { cs = RTColorManager.getTemporalColorScale();   }
      else if (colorscale_3_rbmi.isSelected()) { cs = RTColorManager.getTemporalColorScale();   equal_cs = true; }
      else if (colorscale_4_rbmi.isSelected()) { cs = null; }

      // Special colorscale for the difference
      if (render_value == RENDER_VALUE.DIFFERENCE) { cs = new BrewerColorScale(BrewerColorScale.BrewerType.DIVERGING, 11); }

      // Create the render context based on the user's parameters
      RenderContext myrc = null;
      if (bs != null && field_0 != null && field_1 != null) { myrc = new RenderContext(id, bs, count_by, color_by, render_value, cs, equal_cs, field_0, field_1, drawLabels(), getWidth(), getHeight()); }
      return myrc;
    }
    
    /**
     * RenderContext implementation for the correlation matrix
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by,

      /**
       * First field for correlation
       */
			    field0,
      /**
       * Second field for correlation
       */
			    field1;
      /**
       * ColorScale to use for this rendering
       */
      ColorScale            cs;

      /**
       * Equal colorscale -- monotonically increasing 
       */
      boolean               equal_cs = false;

      /**
       * Draw the labels
       */
      boolean               draw_labels = true;

      /**
       * Render which value
       */
      RENDER_VALUE          render_value = RENDER_VALUE.CHI_SQUARE;

      /**
       * Count context
       */
      BundlesCounterContext counter_context,

      /**
       * Counter context for the rows
       */
                            row_counter_context,

      /**
       * Counter context for the columns
       */
                            col_counter_context;

      /**
       * Lookup for a record to the string keys
       */
      Map<Bundle,Set<String>> bundle_to_skeys = new HashMap<Bundle,Set<String>>();

      /**
       * Lookup for a string key to the bundles
       */
      Map<String,Set<Bundle>> skey_to_bundles = new HashMap<String,Set<Bundle>>();

      /**
       * Lookup for a string key to the geometry
       */
      Map<String,Rectangle2D> skey_to_geom    = new HashMap<String,Rectangle2D>();

      /**
       * Lookup for a geometry to a string key
       */
      Map<Rectangle2D,String> geom_to_skey    = new HashMap<Rectangle2D,String>();

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param render_value       render value
       *@param cs                 colorscale to use for the rendering
       *@param equals_cs          use equal steps in a colorscale
       *@param field0             first field for correlation
       *@param field1             second field for correlation
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, RENDER_VALUE render_value, ColorScale cs, boolean equal_cs, String field0, String field1, boolean draw_labels, int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
	this.count_by           = count_by;
	this.color_by           = color_by;
	this.render_value       = render_value;
	this.cs                 = cs;
        this.equal_cs           = equal_cs;
	this.field0             = field0;
	this.field1             = field1;
	this.draw_labels        = draw_labels;

        double summed           = 0.0;

	// Create the sorts -- use the root so that the visualization doesn't jump around
	validateSorts(field0, field1);

	counter_context     = new BundlesCounterContext(bs, count_by, color_by);
	row_counter_context = new BundlesCounterContext(bs, count_by, color_by);
	col_counter_context = new BundlesCounterContext(bs, count_by, color_by);

	Iterator<Tablet> it_tab = bs.tabletIterator();
	while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
	  //
	  // For each tablet, determine if it can contribute
	  //
          Tablet  tablet           = it_tab.next();
	  boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by),
	          tablet_field_0   = KeyMaker.tabletCompletesBlank(tablet, field0),
		  tablet_field_1   = KeyMaker.tabletCompletesBlank(tablet, field1);
          if (tablet_can_count && tablet_field_0 && tablet_field_1) {
	    //
	    // Go through the individual records
	    //
            Iterator<Bundle> it_bun = tablet.bundleIterator();
	    KeyMaker         km0    = new KeyMaker(tablet, field0),
	                     km1    = new KeyMaker(tablet, field1);
	    while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
	      Bundle bundle     = it_bun.next(); 
	      String f0_strs[]  = km0.stringKeys(bundle),
	             f1_strs[]  = km1.stringKeys(bundle);
	      if (f0_strs != null && f1_strs != null) {
	        bundle_to_skeys.put(bundle, new HashSet<String>());
                for (int i=0;i<f0_strs.length;i++) {
		  String key0 = Utils.encToURL(f0_strs[i]); // key0 will be row
	          for (int j=0;j<f1_strs.length;j++) {
		    String key1 = Utils.encToURL(f1_strs[j]); // key1 will be column
		    String skey = key0 + BundlesDT.DELIM + key1;

                    double v;
		    // Cell update
		    summed += counter_context.    count(bundle, skey); skeys.add(skey);
		    // Row update
		    row_counter_context.count(bundle, key0); rows.add(key0);
		    // Col update
		    col_counter_context.count(bundle, key1); cols.add(key1);

		    // Track skey to bundles and vice versa
		    if (skey_to_bundles.containsKey(skey) == false) skey_to_bundles.put(skey, new HashSet<Bundle>());
		    skey_to_bundles.get(skey).add(bundle);
		    bundle_to_skeys.get(bundle).add(skey);
		  }
	        }
              } else { addToNoMappingSet(bundle); }
	    }
	  } else { addToNoMappingSet(tablet); }
	}

	//
	// Compute the chi-squared test
	//

	// Over the rows
	Iterator<String> it_rows = rows.iterator(); while (it_rows.hasNext()) {
	  String row = it_rows.next();

	  // Over the cols
	  Iterator<String> it_cols = cols.iterator(); while (it_cols.hasNext()) {
	    String col = it_cols.next();

	    String skey = row + BundlesDT.DELIM + col; if (skeys.contains(skey)) {
	      double expected = row_counter_context.total(row) * col_counter_context.total(col) / summed;
	      double value    = 0.0;

	      switch (render_value) {
	        case CHI_SQUARE: double chi_sq   = (counter_context.total(skey) - expected);
	                         chi_sq   = (chi_sq * chi_sq) / expected;
				 value = chi_sq;
                                 // System.err.println("For " + row + " @ " + col + " | expected=" + expected + " | observed = " + counter_context.total(skey) + " | chi_sq = " + chi_sq);
				 break;

		case DIFFERENCE: value = counter_context.total(skey) - expected;
		                 break;

		case EXPECTED:   value = expected;
		                 break;
	      }

	      chi_map.put(skey, value); chi_sq_sum += value; if (equal_cs || cs == null) values_set.add(value);
              if (value > chi_sq_max) chi_sq_max = value;
              if (value < chi_sq_min) chi_sq_min = value;
              // System.err.println("" + row + "[" + row_counter_context.total(row) + "] x " + col + "[" + col_counter_context.total(col) + "] ... " + "[sum = " + summed + "] ... chi_sq = " + chi_sq);
	    }
	  }
	}

	// If equal colorscale, sort the values and then assign color lookups
	if (equal_cs)           {
	  List<Double> sort = new ArrayList<Double>(); 
	  sort.addAll(values_set);
	  Collections.sort(sort);
	  for (int i=0;i<sort.size();i++) equal_lu.put(sort.get(i), ((float) i)/(sort.size()-1));
	} else if (cs == null) {
          BrewerColorScale scales[] = new BrewerColorScale[3];
	  scales[0] = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 7, 1);
	  scales[1] = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 7, 0);
	  scales[2] = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 7, 2);
          cluster_colors  = new ClusterColors(scales, values_set);
        }
      }

      /**
       * Cluster colors (prototype, beta)
       */
      ClusterColors cluster_colors;

      /**
       * Equal color scale lookups
       */
      Map<Double,Float> equal_lu = new HashMap<Double,Float>();

      /** 
       * Chi-Squared Sum ... X^2 (only applies in the CHI_SQUARE render mode)
       */
      double chi_sq_sum = 0.0;

      /**
       * Degree of freedom for the test (only applies in the CHI_SQUARE render mode)
       */
      double degree_of_freedom = (field0_sort.size() - 1) * (field1_sort.size() - 1);

      /**
       * All the keys created for this render
       */
      Set<String> skeys = new HashSet<String>(),

      /**
       * Row keys
       */
                  rows  = new HashSet<String>(),

      /**
       * Column keys
       */
		  cols  = new HashSet<String>();

      /**
       * Chi squared test values
       */
      Map<String,Double> chi_map = new HashMap<String,Double>();
	
      /**
       * Values set for all the chi_map calculations -- used for equal colorscale
       */
      Set<Double> values_set = new HashSet<Double>();

      /**
       * Chi squared max
       */
      double chi_sq_max = Double.NEGATIVE_INFINITY,

      /**
       * Chi squared min
       */
             chi_sq_min = Double.POSITIVE_INFINITY;

      /**
       * Text height
       */
      int txt_h,

      /**
       * Cell width
       */
          cell_w,

      /**
       * Cell height
       */
	  cell_h,

      /**
       *  Maximum width for row labels
       */
          max0_w,

      /**
       * Maximum width for column labels (they are rotated 90 degrees...)
       */
          max1_w,

      /**
       * Upper left hand corner of the table
       */
	  table_x,

      /**
       * Upper left hand corner of the table
       */
	  table_y;

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (base_bi == null) {
	 Graphics2D g2d = null;
	 try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
	  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	  RTColorManager.renderVisualizationBackground(base_bi, g2d);

          txt_h = Utils.txtH(g2d, "0"); cell_w = 2; cell_h = 2;
          max0_w = 0; 
          max1_w = 0; 

	  if (draw_labels) {
            for (int i=0;i<field0_sort.size();i++) { int txt_w = Utils.txtW(g2d, Utils.decFmURL(field0_sort.get(i))); if (txt_w > max0_w) max0_w = txt_w; }
	    for (int i=0;i<field1_sort.size();i++) { int txt_w = Utils.txtW(g2d, Utils.decFmURL(field1_sort.get(i))); if (txt_w > max1_w) max1_w = txt_w; }
	    cell_w = cell_h = txt_h+2;
          } else {
	    max0_w = max1_w = 4;
            cell_w = cell_h = 4;
	  }

          table_x = max0_w + 4;
          table_y = max1_w + 4;

	  // Draw the labels
          if (draw_labels) {
	    g2d.setColor(RTColorManager.getColor("label", "default"));
            for (int i=0;i<field0_sort.size();i++) g2d.drawString(Utils.decFmURL(field0_sort.get(i)), 1, table_y + (i+1) * cell_h - 1);
	    for (int i=0;i<field1_sort.size();i++) Utils.drawRotatedString(g2d, Utils.decFmURL(field1_sort.get(i)), table_x + (i+1) * cell_w - 1, table_y - 1);
	  }

	  // Draw the cell colors
	  for (int y_i=0;y_i<field0_sort.size();y_i++) {
	    int y = table_y + y_i * cell_h;
	    for (int x_i=0;x_i<field1_sort.size();x_i++) {
	      int    x      = table_x + x_i * cell_w;

	      String skey = field0_sort.get(y_i) + BundlesDT.DELIM + field1_sort.get(x_i);
	      if (chi_map.containsKey(skey)) {
	        double      chi_sq = chi_map.get(skey);

		// Set the color
		if (render_value == RENDER_VALUE.DIFFERENCE) {
                  // 0...4 are negative // 5 is neurtal // 6...10 are positive
		  int cs_index;
		  if      (chi_sq <  0.0) { cs_index = (int) Math.ceil(5 - 5 * (chi_sq / chi_sq_min)); }
		  else if (chi_sq >  0.0) { cs_index = (int) Math.ceil(5 + 5 * (chi_sq / chi_sq_max)); }
		  else                    { cs_index = 5; }
		  g2d.setColor(((BrewerColorScale) cs).atIndex(cs_index));
		} else if (equal_cs)                         {
                  g2d.setColor(cs.at(equal_lu.get(chi_sq)));
		} else if (cs == null)                       {
		  g2d.setColor(cluster_colors.at(chi_sq));
		} else                                       {
		  double norm   = (chi_sq - chi_sq_min) / (chi_sq_max - chi_sq_min);
	          g2d.setColor(cs.at((float) norm));
                }

		// Create the shape and fill
	        Rectangle2D rect   = new Rectangle2D.Float(x, y, cell_w-1, cell_h-1);
	        g2d.fill(rect);

		// Keep track of the correlation to records
                skey_to_geom.put(skey, rect);
		geom_to_skey.put(rect, skey);
	      }
	    }
	  }

	  // Render the color scale
          int cs_w = getRCWidth() - 10; 
	  int cs_y = table_y + field0_sort.size() * cell_h + 3;
	  int la_y = cs_y + txt_h;

	  if (cs != null) {
            if (cs_w > 10) {
	      for (int x=0;x<cs_w;x++) {
	        float f = ((float) x)/((float) (cs_w - 1));
	        g2d.setColor(cs.at(f));
	        g2d.drawLine(x + 5, cs_y, x + 5, la_y);
	      }
	    }
	  } else {
            // Cluster colors colorscale
	    if (cs_w > 10) { cluster_colors.renderColorScale(g2d, 5, cs_y, cs_w, txt_h); }
	  }

          // Draw the numerical scale
	  g2d.setColor(RTColorManager.getColor("label", "default")); String s;
	  s = Utils.humanReadableDouble(chi_sq_min); g2d.drawString(s, 5,                            la_y + txt_h + 2);
	  s = Utils.humanReadableDouble(chi_sq_max); g2d.drawString(s, 5 + cs_w - Utils.txtW(g2d,s), la_y + txt_h + 2);

          // For Chi-Square mode, print the X^2 value and the degree of freedom
	  if (render_value == RENDER_VALUE.CHI_SQUARE) {
	    int y0 = table_y + field0_sort.size() * cell_h + 3;
	    int y1 = y0 + txt_h;
	    g2d.setColor(RTColorManager.getColor("label", "default"));
	    s = "X^2 = " + chi_sq_sum + " ... DoF = " + degree_of_freedom;
	    g2d.drawString(s, 5, y1 + 2*(txt_h+2));
	  }
	 } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }
  }
}

